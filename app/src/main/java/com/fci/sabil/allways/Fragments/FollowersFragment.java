package com.fci.sabil.allways.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.fci.sabil.allways.Adapters.FollowersAdapter;
import com.fci.sabil.allways.Database_Connection.Connection;
import com.fci.sabil.allways.Database_Connection.ConnectionPostListener;
import com.fci.sabil.allways.Items.Follower;
import com.fci.sabil.allways.MainActivity;
import com.fci.sabil.allways.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ahmed on 27-Apr-16.
 */
public class FollowersFragment extends Fragment {

    private int id;
    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        id = MainActivity.getId();

        HashMap<String,String> params = new HashMap<String,String>();
        params.put("followed_id",String.valueOf(id));
        Connection conn = new Connection(params, new ConnectionPostListener() {
            @Override
            public void doAction(String result) {
                try {

                    Snackbar.make(container, "||"+result+"||", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                    JSONObject reader = new JSONObject(result);
                    id = Integer.parseInt(reader.getString("id"));
                    ArrayList<Follower> followers = new ArrayList<Follower>();
                    JSONArray jsonArray = new JSONArray (reader);
                    followers = Follower.fromJson(jsonArray);

                    FollowersAdapter adapter = new FollowersAdapter(getActivity(), followers);
                    ListView listView = (ListView) getView().findViewById(R.id.followers);
                    listView.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        conn.execute("http://allways-sabil.rhcloud.com/FCISquare/rest/getFollowers");

        return inflater.inflate(R.layout.fragment_followers, container, false);
    }
}
