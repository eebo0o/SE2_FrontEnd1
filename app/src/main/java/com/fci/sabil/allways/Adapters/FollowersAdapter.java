package com.fci.sabil.allways.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.fci.sabil.allways.Items.Follower;
import com.fci.sabil.allways.R;

import java.util.ArrayList;

/**
 * Created by basse on 5/9/2016.
 */
public class FollowersAdapter extends ArrayAdapter<Follower> {

    public FollowersAdapter(Context context, ArrayList<Follower> followers) {
        super(context, 0, followers);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Follower follower = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.follower, parent, false);
        }
        // Lookup view for data population
        TextView name = (TextView) convertView.findViewById(R.id.follower_name);
        TextView email = (TextView) convertView.findViewById(R.id.follower_email);
        // Populate the data into the template view using the data object
        name.setText(follower.name);
        email.setText(follower.email);
        // Return the completed view to render on screen
        return convertView;
    }
}
