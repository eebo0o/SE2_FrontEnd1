package com.fci.sabil.allways.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.fci.sabil.allways.Adapters.PlacesAdapter;
import com.fci.sabil.allways.Database_Connection.Connection;
import com.fci.sabil.allways.Database_Connection.ConnectionPostListener;
import com.fci.sabil.allways.Items.Place;
import com.fci.sabil.allways.MainActivity;
import com.fci.sabil.allways.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ahmed on 27-Apr-16.
 */
public class CheckinsFragment extends Fragment {

    private int id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        id = MainActivity.getId();

        HashMap<String,String> params = new HashMap<String,String>();
        params.put("followed_id",String.valueOf(id));
        Connection conn = new Connection(params, new ConnectionPostListener() {
            @Override
            public void doAction(String result) {
                try {

                    JSONObject reader = new JSONObject(result);
                    id = Integer.parseInt(reader.getString("id"));
                    JSONArray jsonArray = new JSONArray (reader);

                    ArrayList<Place> places = Place.fromJson(jsonArray);
                    PlacesAdapter adapter = new PlacesAdapter(getActivity(), places);

                    ListView listView = (ListView) getView().findViewById(R.id.places);
                    listView.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        conn.execute("http://allways-sabil.rhcloud.com/FCISquare/rest/showplaces");

        return inflater.inflate(R.layout.fragment_checkin, container, false);

    }
}
