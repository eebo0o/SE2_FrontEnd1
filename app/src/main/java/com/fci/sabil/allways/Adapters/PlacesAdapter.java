package com.fci.sabil.allways.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.fci.sabil.allways.Items.Place;
import com.fci.sabil.allways.R;

import java.util.ArrayList;

/**
 * Created by basse on 5/9/2016.
 */
public class PlacesAdapter extends ArrayAdapter<Place> {

    public PlacesAdapter(Context context, ArrayList<Place> places) {
        super(context, 0, places);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Place place = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.place, parent, false);
        }
        // Lookup view for data population
        TextView name = (TextView) convertView.findViewById(R.id.place_name);
        TextView lon = (TextView) convertView.findViewById(R.id.place_longitude);
        TextView lat = (TextView) convertView.findViewById(R.id.place_latitude);
        TextView rate = (TextView) convertView.findViewById(R.id.place_rate);

        // Populate the data into the template view using the data object
        name.setText(place.name);
        lon.setText(place.longitude);
        lat.setText(place.latitude);
        rate.setText(place.rate);
        // Return the completed view to render on screen
        return convertView;}
}
