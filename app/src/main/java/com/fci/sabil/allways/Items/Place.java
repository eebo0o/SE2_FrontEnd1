package com.fci.sabil.allways.Items;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by basse on 5/9/2016.
 */
public class Place {
    public String name;
    public String latitude;
    public String longitude;
    public String rate;

    public Place(String name, String latitude, String longitude, String rate) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.rate = rate;
    }

    public Place(JSONObject jsonObject) {
        try {
            this.name = jsonObject.getString("name");
            this.longitude = jsonObject.getString("lon");
            this.latitude = jsonObject.getString("lat");
            this.rate = jsonObject.getString("rate");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Place> fromJson(JSONArray jsonObjects)
    {
        ArrayList<Place> places = new ArrayList<Place>();
        for (int i = 0; i < jsonObjects.length(); i++) {
            try {
                places.add(new Place(jsonObjects.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return places;
    }


}
