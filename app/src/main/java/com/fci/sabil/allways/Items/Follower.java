package com.fci.sabil.allways.Items;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by basse on 5/9/2016.
 */
public class Follower {

    public String name;
    public String email;

    public Follower(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public Follower(JSONObject jsonObject) {
        try {
            this.name = jsonObject.getString("name");
            this.email = jsonObject.getString("email");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Follower> fromJson(JSONArray jsonObjects) {
        ArrayList<Follower> followers= new ArrayList<Follower>();
        for (int i = 0; i < jsonObjects.length(); i++) {
            try {
                followers.add(new Follower(jsonObjects.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return followers;
    }
}
